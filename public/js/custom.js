/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 44);
/******/ })
/************************************************************************/
/******/ ({

/***/ 44:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(45);


/***/ }),

/***/ 45:
/***/ (function(module, exports) {

function start_loading_process() {
    $("#loading_process").addClass('show');
}

function finish_loading_process() {
    $("#loading_process").removeClass('show');
}

$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("html").on("click", "#btn-update-profile", function () {
        //        send new profile data to server
        var form_data = new FormData();
        var email = $("#email").val();
        var password = $("#password").val();
        var repeat_password = $("#repeat-password").val();

        form_data.append('email', email);
        form_data.append('password', password);
        form_data.append('password_confirmation', repeat_password);
        form_data.append('avatar', $("#avatar")[0].files[0]);

        $.ajax({
            url: $("#form-edit-profile").attr("action"),
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function beforeSend() {
                start_loading_process();
            },
            complete: function complete() {
                finish_loading_process();
            },
            success: function success(data) {
                var msg = "";
                var avatarUrl = data.avatarUrl;

                if (avatarUrl) $("#avatar_img").css('background', 'url(' + avatarUrl + ')');

                for (var i = 0; i < data.messages.length; i++) {
                    msg += data.messages[i] + "\n";
                }$("#modal_profile_update").modal('show');
                $("#modal_profile_update").find("#content").html(msg);
            }
        });

        return false;
    });

    $("body").on("click", "#btn-add-task", function () {
        var name = $("#name").val();
        var description = $("#description").val();

        $.ajax({
            type: "post",
            url: $("#form-create-task").attr("action"),
            dataType: "json",
            data: {
                name: name,
                description: description
            },
            beforeSend: function beforeSend() {
                start_loading_process();
            },
            complete: function complete() {
                finish_loading_process();
            },
            success: function success(data) {
                var msg = "";

                for (var i = 0; i < data.messages.length; i++) {
                    msg += data.messages[i] + "\n";
                }$("#modal_create_task").modal('show');
                $("#modal_create_task").find("#content").html(msg);
            }
        });

        return false;
    });
});

/***/ })

/******/ });