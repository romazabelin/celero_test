<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function profile(Request $request)
    {
//        Profile page. if "GET" - return view with current user info, if "POST" - update user profile (validate email, password etc., upload avatar)
        if ($request->isMethod('post'))
        {
            $email = $request->get('email');
            $password = $request->get('password');
            $rulesEmail = array(
                'email' => 'unique:users,email,' . Auth::user()->id
            );
            $rulesPassword = array(
                'password' => 'required|min:6|confirmed'
            );
            $messages = array();
            $valid = true;
            $avatarUrl = "";

            $validator = Validator::make($request->all(), $rulesEmail);

            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                array_push($messages, Lang::get('profile.email_is_not_valid'));
                $valid = false;
            }
            else if ($validator->fails())
            {
                array_push($messages, Lang::get('profile.error_email_exist'));
                $valid = false;
            }
            else
            {
                Auth::user()->email = $email;
                Auth::user()->save();
            }

            if ($password)
            {
                $validator = Validator::make($request->all(), $rulesPassword);

                if ($validator->fails()) {
                    array_push($messages, Lang::get('profile.error_password'));
                    $valid = false;
                } else {
                    Auth::user()->password = Hash::make($password);
                    Auth::user()->save();
                }
            }

            if ($request->hasFile('avatar'))
            {
                $avatar = $request->file('avatar');
                $avatarName = $avatar->getClientOriginalName();
                $pathUpload = public_path('images/avatars/' . Auth::user()->id);
                if ($avatar->move($pathUpload, $avatarName))
                {
                    $avatarUrl = url('images/avatars/' . Auth::user()->id) . "/" . $avatarName;
                    Auth::user()->avatar = $avatarName;
                    Auth::user()->save();
                }

            }

            if ($valid)
                array_push($messages, Lang::get('profile.update_profile_successful'));

            return response()->json([
                'messages' => $messages,
                'avatarUrl' => $avatarUrl
            ]);
        }

        return view('profile.edit');
    }

}
