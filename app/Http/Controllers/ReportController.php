<?php

namespace App\Http\Controllers;

use App\Repositories\TaskRepository;
use Faker\Provider\cs_CZ\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{

    protected $taskRepository;

    public function __construct(TaskRepository $taskRepository)
    {
        $this->middleware('auth');
        $this->taskRepository = $taskRepository;
    }

    public function index()
    {
        return view('reports.index');
    }

    public function getReport(Request $request)
    {
//        get report for user in given period. Return only finished tasks
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate') . " 23:59:00";

        return response()->json(['tasks' => $this->taskRepository->getTasksForReport(Auth::user(), $startDate, $endDate)]);
    }
}
