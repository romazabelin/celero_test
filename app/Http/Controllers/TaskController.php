<?php

namespace App\Http\Controllers;

use App\Repositories\TaskRepository;
use App\Task;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    protected $taskRepository;

    public function __construct(TaskRepository $taskRepository)
    {
        $this->middleware('auth');
        $this->taskRepository = $taskRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tasks = $this->taskRepository->getTasksForUser(Auth::user());

        return view('tasks.index', [
            'tasks' => $tasks
        ]);
    }

    public function getTasks()
    {
//        get data for render task list
        return response()->json([
            'tasks' => $this->taskRepository->getTasksForUser(Auth::user()),
            'statuses' => Task::$statuses,
            'statusInProgress' => Task::statusInProgress,
            'statusPause' => Task::statusPause,
            'statusFinish' => Task::statusFinish
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        create new task
        if ($request->isMethod('post'))
        {
            $result = $this->manageTask($request, false);

            return response()->json($result);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = $this->taskRepository->getTask(Auth::user(), $id);

        if (!$task)
            return redirect(route('tasks'));

        return view('tasks.edit', [
            'task' => $task
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        update task
        $result = $this->manageTask($request, $id);

        return response()->json($result);
    }

    public function stopTask(Request $request, $id)
    {
//        here we set status finish for task and calculate elapsed time
        $task = $this->taskRepository->getTask(Auth::user(), $id);

        if ($task)
        {
            if ($task->status == Task::statusInProgress)
                $this->taskRepository->changeElapsedTime($task);
            $this->taskRepository->stopTask(Auth::user(), $id);
        }
    }

    public function pauseTask(Request $request, $id)
    {
//        here we set status pause for task and calculate elapsed time
        $task = $this->taskRepository->getTask(Auth::user(), $id);

        if ($task)
        {
            $this->taskRepository->changeElapsedTime($task);
            $this->taskRepository->pauseTask(Auth::user(), $id);
        }
    }

    public function startTask(Request $request, $id)
    {
//        here we start task, get tasks with status in progress then change in on pause and update elapsed time automatically
        $task = $this->taskRepository->getTask(Auth::user(), $id);

        if ($task)
        {
            $tasksInProgress = $this->taskRepository->getTasksInProgress(Auth::user(), $id);

            foreach ($tasksInProgress as $taskItem)
                $this->taskRepository->changeElapsedTime($taskItem);

            $this->taskRepository->startTask(Auth::user(), $id);
            $this->taskRepository->pauseTasks(Auth::user(), $id);

        }
    }

    public function manageTask(Request $request, $id) {
//        create and eidt task. If $id param exist - edit. Also validate data
        $name = $request->get('name');
        $description = $request->get('description');
        $messages = array();
        $valid = true;
        $taskToEdit = $this->taskRepository->getTask(Auth::user(), $id);

        $validator = Validator::make($request->all(), ['name' => 'required|max:100']);

        if ($validator->fails())
        {
            $valid = false;
            array_push($messages, Lang::get('task.error_name'));
        }

        if ($valid)
        {
            if ($id && $taskToEdit)
            {
                $this->taskRepository->updateTask(Auth::user(), $taskToEdit->id, $name, $description);
                array_push($messages, Lang::get('task.task_updated'));
            }
            else if ($id && !$taskToEdit)
            {
                array_push($messages, Lang::get('task.error_permission'));
            }
            else
            {
                $task = $this->taskRepository->createTask(Auth::user(), $name, $description);
                ($task->id) ? array_push($messages, Lang::get('task.success_create_task')) : array_push($messages, Lang::get('task.wrong_create_task'));
            }
        }

        return array(
            'messages' => $messages
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        remove task from DB
        $task = $this->taskRepository->getTask(Auth::user(), $id);

        if ($task)
            $task->delete();

        return redirect(route('tasks'));
    }
}
