<?php
namespace App\Repositories;

use App\Task;
use App\User;

class TaskRepository
{
    public function createTask(User $user, $name, $description)
    {
//        create task for user with params
        return $user->tasks()->create([
            'name' => $name,
            'description' => $description
        ]);
    }

    public function changeElapsedTime(Task $task)
    {
//        change elapsed time for task
        $elapsedSeconds = time() - strtotime($task->last_start_time_at);
        $task->elapsed_seconds += $elapsedSeconds;
        $task->save();
    }

    public function getTasksForReport(User $user, $startDate, $endDate)
    {
//        get finished tasks for report
        return $user->tasks()->where([
            ['status', '=', Task::statusFinish]
        ])->whereBetween('finish_time_at', [$startDate, $endDate])->get();
    }

    public function updateTask(User $user, $id, $name, $description)
    {
        $user->tasks()
            ->where('id', $id)
            ->update([
                'name' => $name,
                'description' => $description
            ]);
    }

    public function startTask(User $user, $id)
    {
        $user->tasks()
            ->where('id', $id)
            ->update([
                'last_start_time_at' => date("Y-m-d H:i:s", time()),
                'status' => Task::statusInProgress
            ]);
    }

    public function getTasksInProgress(User $user, $id)
    {
        return $user->tasks()->where([
            ['id', '<>', $id],
            ['status', '=', Task::statusInProgress]
        ])->get();
    }

    public function pauseTasks(User $user, $id)
    {
        $user->tasks()->where([
            ['id', '<>', $id],
            ['status', '=', Task::statusInProgress]
        ])->update([
            'status' => Task::statusPause
        ]);
    }

    public function stopTask(User $user, $id)
    {
        $user->tasks()
            ->where('id', $id)
            ->update([
                'status' => Task::statusFinish,
                'finish_time_at' => date("Y-m-d H:i:s", time())
            ]);
    }

    public function pauseTask(User $user, $id)
    {
        $user->tasks()
            ->where('id', $id)
            ->update([
                'status' => Task::statusPause
            ]);
    }

    public function getTasksForUser(User $user)
    {
        return $user->tasks()->get();
    }

    public function getTask(User $user, $id)
    {
        return Task::where([
            ['user_id' , '=', $user->id],
            ['id', '=', $id]
        ])->first();
    }
}