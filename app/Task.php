<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['name', 'description'];

    const statusInProgress = 1;

    const statusPause = 2;

    const statusFinish = 3;

    static $statuses = array(
        self::statusInProgress => 'In progress',
        self::statusPause => 'Paused',
        self::statusFinish => 'Finished',
    );

    public function user()
    {
        $this->belongsTo(User::class);
    }
}
