function start_loading_process() {
    $("#loading_process").addClass('show');
}

function finish_loading_process() {
    $("#loading_process").removeClass('show');
}

$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("html").on("click", "#btn-update-profile", function() {
//        send new profile data to server
        var form_data = new FormData();
        var email = $("#email").val();
        var password = $("#password").val();
        var repeat_password = $("#repeat-password").val();

        form_data.append('email', email);
        form_data.append('password', password);
        form_data.append('password_confirmation', repeat_password);
        form_data.append('avatar', $("#avatar")[0].files[0]);

        $.ajax({
            url: $("#form-edit-profile").attr("action"),
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                start_loading_process();
            },
            complete: function() {
                finish_loading_process();
            },
            success: function(data) {
                var msg = "";
                var avatarUrl = data.avatarUrl;

                if (avatarUrl)
                    $("#avatar_img").css('background', 'url(' + avatarUrl + ')');

                for (var i = 0; i < data.messages.length; i++)
                    msg += data.messages[i] + "\n";

                $("#modal_profile_update").modal('show');
                $("#modal_profile_update").find("#content").html(msg);
            }
        })

        return false;
    })

    $("body").on("click", "#btn-add-task", function() {
        var name = $("#name").val();
        var description = $("#description").val();

        $.ajax({
            type: "post",
            url: $("#form-create-task").attr("action"),
            dataType: "json",
            data: {
                name: name,
                description: description
            },
            beforeSend: function() {
                start_loading_process();
            },
            complete: function() {
                finish_loading_process();
            },
            success: function(data) {
                var msg = "";

                for (var i = 0; i < data.messages.length; i++)
                    msg += data.messages[i] + "\n";

                $("#modal_create_task").modal('show');
                $("#modal_create_task").find("#content").html(msg);
            }
        })

        return false;
    })
})