<?php

return [
    /*
   |--------------------------------------------------------------------------
   | Profile Language Lines
   |--------------------------------------------------------------------------
   */

    'password' => 'Password',
    'repeat_password' => 'Repeat password',
    'save' => 'Save',
    'error_email_exist' => 'Email already exist',
    'error_password' => 'The password must be at least 6 characters or the password confirmation does not match',
    'update_profile_successful' => 'Profile was updated',
    'email_is_not_valid' => 'Email is not valid'
];