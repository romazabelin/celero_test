<?php

return [
    'column_id' => '#',
    'create' => 'Create task',
    'name' => 'Name',
    'status' => 'Status',
    'description' => 'Description',
    'save' => 'Save',
    'success_create_task' => 'Task was created',
    'wrong_create_task' => 'Some problems. Try later',
    'error_name' => 'Name is required. Max length - 100',
    'actions' => 'Actions',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'error_permission' => 'You do not have permission to this task',
    'task_updated' => 'Task was updated',
    'start' => 'Start',
    'stop' => 'Stop',
    'pause' => 'Pause',
    'elapsed_time' => 'Elapsed time(min.)'
];