<?php
return [
    'reports' => 'Reports',
    'start_date' => 'Start date',
    'end_date' => 'End date',
    'no_tasks' => 'Nothing to show',
    'total_in_min' => 'Total(minutes)'
];