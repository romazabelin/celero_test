@extends('layouts.app')

@section('content')

@include('modals.create_task')

<div class="container">
    <div class="row">
        <form method="post" action="{{ route('task.store') }}" id="form-create-task">
            @csrf

            <div class="form-group">
                <input type="text" class="form-control" placeholder="{!! trans('task.name') !!}" id="name" maxlength="100">
            </div>

            <div class="form-group">
                <textarea class="form-control" placeholder="{!! trans('task.description') !!}" id="description" maxlength="1000"></textarea>
            </div>

            <div class="form-group">
                <input type="submit" value="{!! trans('task.save') !!}" class="btn btn-primary" id="btn-add-task">
            </div>
        </form>
    </div>
</div>
@endsection
