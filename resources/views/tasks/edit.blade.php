@extends('layouts.app')

@section('content')

@include('modals.create_task')

<div class="container">
    <div class="row">
        <form action="{{ route('task.update', ['id' => $task->id]) }}" method="post" id="form-create-task">
            @csrf

            <div class="form-group">
                <input type="text" class="form-control" placeholder="{!! trans('task.name') !!}" id="name" maxlength="100" value="{!! $task->name !!}">
            </div>

            <div class="form-group">
                <textarea class="form-control" placeholder="{!! trans('task.description') !!}" id="description" maxlength="1000">{!! $task->description !!}</textarea>
            </div>

            <div class="form-group">
                <input type="submit" value="{!! trans('task.save') !!}" class="btn btn-primary" id="btn-add-task">
            </div>
        </form>
    </div>
</div>
@endsection
