@extends('layouts.app')

@section('content')

<div class="container" id="app">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('task.create') }}">
                {!! trans('task.create') !!}
            </a>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>{!! trans('task.column_id') !!}</th>
                        <th>{!! trans('task.name') !!}</th>
                        <th>{!! trans('task.status') !!}</th>
                        <th>{!! trans('task.elapsed_time') !!}</th>
                        <th>{!! trans('task.actions') !!}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <tr v-for="task in tasks">
                    <td>@{{task.id}}</td>
                    <td>@{{task.name}}</td>
                    <td>@{{statuses[task.status]}}</td>
                    <td>@{{ getMinutes(task.elapsed_seconds) }}</td>
                    <td>
                        <a :href="'/edit_task/' + task.id">
                            <input type="button" value="{!! trans('task.edit') !!}" class="btn btn-primary">
                        </a>
                        <a :href="'/destroy_task/' + task.id">
                            <input type="button" value="{!! trans('task.delete') !!}" class="btn btn-danger">
                        </a>
                    </td>
                    <td>
                        <a v-if="task.status != statusInProgress && task.status != statusFinish" :href="'/start_task/' + task.id" v-on:click.prevent="startTask">
                            <input type="button" class="btn btn-info" value="{!! trans('task.start') !!}">
                        </a>

                        <a v-if="task.status == statusInProgress" :href="'/pause_task/' + task.id" v-on:click.prevent="pauseTask">
                            <input type="button" class="btn btn-success" value="{!! trans('task.pause') !!}">
                        </a>

                        <a v-if="task.status == statusInProgress || task.status == statusPause" :href="'/stop_task/' + task.id" v-on:click.prevent="stopTask">
                            <input type="button" class="btn btn-warning" value="{!! trans('task.stop') !!}">
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        var app = new Vue({
            el: '#app',
            data: {
                tasks: [],
                statuses: [],
                statusInProgress: null,
                statusPause: null,
                statusFinish: null
            },
            created: function() {
                this.getTasks();
            },
            methods: {
                getTasks: function() {
                    //get task and other data
                    var self = this;

                    $.get("{{ route('task.list') }}", function(data) {
                        self.tasks = data.tasks;
                        self.statuses = data.statuses;
                        self.statusInProgress = data.statusInProgress;
                        self.statusPause = data.statusPause;
                        self.statusFinish = data.statusFinish
                    })
                },
                startTask: function(e) {
                    //tell server that we want change status
                    var self = this;
                    var link = e.currentTarget.getAttribute('href');

                    $.get(link, function(data) {
                        self.getTasks();
                    })
                },
                pauseTask: function(e) {
                    //tell server that we want change status
                    var self = this;
                    var link = e.currentTarget.getAttribute('href');

                    $.get(link, function(data) {
                        self.getTasks();
                    })
                },
                stopTask: function(e) {
                    //tell server that we want change status
                    var self = this;
                    var link = e.currentTarget.getAttribute('href');

                    $.get(link, function(data) {
                        self.getTasks();
                    })
                },
                getMinutes: function(seconds) {
                    //calculate elapsed time in minutes
                    var minutes = Math.floor(seconds / 60);
                    var secondsLeft = seconds % 60;

                    return minutes + ":" + secondsLeft;
                }
            }
        })
    })
</script>
@endsection