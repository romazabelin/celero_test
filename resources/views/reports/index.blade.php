@extends('layouts.app')

@section('content')
<div class="container" id="app">
    <div class="row">
        <div class="col-md-12">
            <h3>{!! trans('report.reports') !!}</h3>
            <input class="form-control" placeholder="{!! trans('report.start_date') !!}" id="start-date">
            <input class="form-control" placeholder="{!! trans('report.end_date') !!}" id="end-date">
            <input type="button" class="btn btn-info" value="Get" v-on:click.prevent="getReport">

            <div class="margin-top-50">
                <div v-if="tasks.length > 0">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>{!! trans('task.column_id') !!}</th>
                            <th>{!! trans('task.name') !!}</th>
                            <th>{!! trans('task.elapsed_time') !!}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="task in tasks">
                            <td>@{{task.id}}</td>
                            <td>@{{task.name}}</td>
                            <td>@{{getMinutes(task.elapsed_seconds)}}</td>
                        </tr>
                        </tbody>
                    </table>
                    <div>{!! trans('report.total_in_min') !!}: @{{getMinutes(totalTimeElapsed)}}</div>
                </div>
                <div v-else>
                    <h3>{!! trans('report.no_tasks') !!}</h3>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        var app = new Vue({
            el: '#app',
            data: {
                tasks: []
            },
            mounted: function() {
                $("#start-date").datepicker({
                    dateFormat: 'yy-mm-dd',
                    maxDate: 'today'
                });
                $("#end-date").datepicker({
                    dateFormat: 'yy-mm-dd',
                    maxDate: 'today'
                });
            },
            computed: {
                totalTimeElapsed: function() {
                    //calculate total elapsed time
                    if (!this.tasks)
                        return 0;

                    return this.tasks.reduce(function(sum, item) {
                        return sum + Number(item.elapsed_seconds);
                    }, 0);
                }
            },
            methods: {
                getMinutes: function(seconds) {
                    //calculate elapsed time in minutes
                    var minutes = Math.floor(seconds / 60);
                    var secondsLeft = seconds % 60;

                    return minutes + ":" + secondsLeft;
                },
                getReport: function() {
                    //get report from server
                    var self = this;

                    $.ajax({
                        type: "post",
                        url: "{{route('report.get')}}",
                        dataType: "json",
                        data: {
                            startDate: $("#start-date").val(),
                            endDate: $("#end-date").val()
                        },
                        beforeSend: function() {
                        },
                        complete: function() {
                        },
                        success: function(data) {
                            self.tasks = data.tasks
                        }
                    })
                }
            }
        })
    })
</script>
@endsection