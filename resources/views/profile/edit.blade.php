@extends('layouts.app')

@section('content')
<div class="modal fade" id="modal_profile_update">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="content">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{!! trans('modal.close') !!}</button>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <form method="post" action="{{ route('profile') }}" id="form-edit-profile" enctype="multipart/form-data">
            @csrf

            <div id="avatar_img"  style="background: url('{{ (Auth::user()->avatar) ? asset('images/avatars/' . Auth::user()->id . '/' . Auth::user()->avatar) : asset('images/person_circle.png') }}')"></div>

            <div class="form-group">
                <input type="text" class="form-control" value="{{ Auth::user()->email }}" id="email">
            </div>

            <div class="form-group">
                <input type="password" class="form-control" placeholder="{!! trans('profile.password') !!}" id="password">
            </div>

            <div class="form-group">
                <input type="password" class="form-control" placeholder="{!! trans('profile.repeat_password') !!}" id="repeat-password">
            </div>

            <div class="form-group">
                <input type="file" id="avatar" name="">
            </div>

            <div class="form-group">
                <input type="submit" value="{!! trans('profile.save') !!}" class="btn btn-primary" id="btn-update-profile">
            </div>
        </form>
    </div>
</div>
@endsection