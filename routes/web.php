<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::match(['get', 'post'], '/profile', 'ProfileController@profile')->name('profile');//create route for profile, we need GET and POST methods

Route::get('/tasks', 'TaskController@index')->name('tasks');

Route::get('/create_task', 'TaskController@create')->name('task.create');

Route::post('/store_task', 'TaskController@store')->name('task.store');

Route::get('/destroy_task/{id}', 'TaskController@destroy')->name('task.destroy');

Route::get('/edit_task/{id}', 'TaskController@edit')->name('task.edit');

Route::post('/update_task/{id}', 'TaskController@update')->name('task.update');

Route::match(['get', 'post'], '/get_tasks', 'TaskController@getTasks')->name('task.list');

Route::match(['get', 'post'], '/start_task/{id}', 'TaskController@startTask')->name('task.start');

Route::match(['get', 'post'], '/pause_task/{id}', 'TaskController@pauseTask')->name('task.pause');

Route::match(['get', 'post'], '/stop_task/{id}', 'TaskController@stopTask')->name('task.stop');

Route::get('/reports', 'ReportController@index')->name('reports');

Route::match(['get', 'post'], '/get_report', 'ReportController@getReport')->name('report.get');
